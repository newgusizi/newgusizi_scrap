const axios = require('axios');
const db = require('../lib/db_connect');
const utils = require('../utils/utils');


exports.test = async () => {
    const result = await db.getSearchList();
    console.log(`test result`);
    console.log(result);
}


exports.updatePrices = async () => {
    try {
        // TODO:: DB 처리 -> cpu 최신기준 / 정품,벌크 구분X
        let search_list = await db.getSearchList();
        const loopLength = search_list.length;

        for(let i=0; i<loopLength; i++) {
            const type = search_list[i].type;
            const searchName = search_list[i].search_name;
            const validate_keywords = search_list[i].keywords.split(',');

            const res = await getNaverResult(searchName);
            const lower_price_info = _getRowerPrice(res, validate_keywords, type);

            // type 1:cpu, 2:gpu
            // lower_price_info: {lower_price: save_lower_price, lower_market: save_lower_market}
            db.insertSearch(searchName, lower_price_info.lower_price, lower_price_info.lower_market, type);
            await utils.sleep(500);
        }
        
        return 1;
    } catch (e) {
        console.log('getPrices Exception.');
        console.log(e);
        console.log('end---------------------------------------------------------');
        return -1;
    }
}


async function getNaverResult(searchName) {
    try {
        console.log(`search start:: ${searchName}`);

        const encodeSearchName = encodeURIComponent(searchName);
        let referer = `https://search.shopping.naver.com/search/all?query=${encodeSearchName}&frm=NVSHATC&prevQuery=${encodeSearchName}`;
        const url = `https://search.shopping.naver.com/api/search/all?sort=rel&pagingIndex=0pagingSize=40&viewType=list&productSet=total&deliveryFee=&deliveryTypeValue=&frm=NVSHATC&query=${encodeSearchName}&origQuery=${encodeSearchName}&iq=&eq=&xq=`;

        let result = await axios.get(url, {
            'Referer': referer,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36'
        });

        return result;

    } catch (e) {
        console.log(`scrap exception. search name :: ${searchName}`);
        console.log(e);
        console.log('end-----------------------------------------');
        return -1;
    }
}


/**
 * 
 * @param {AxiosResponse} res 
 * @param {Array} validate_keywords 
 * @param {int} type 1: CPU, 2: GPU
 * @returns 
 */
function _getRowerPrice(res, validate_keywords, type) {
    let save_lower_price = Number.MAX_SAFE_INTEGER;
    let save_lower_market = '제품없음';
    let same_price_cnt = 0;

    try {
        if (!res.data.shoppingResult.hasOwnProperty('products')) {
            return {save_lower_price: save_lower_price, save_lower_market: save_lower_market};
        }

        const products_list = res.data.shoppingResult.products;
        const products_list_length = products_list.length;


        for(let i=0; i<products_list_length; i++) {
            let row_price;
            let row_market;

            const category = res.data.shoppingResult.products[i].category3Name;
            const collectableCat = ['그래픽카드','CPU'];

            if (!collectableCat.includes(category)) { continue; }

            const product_name = products_list[i].productName;

            const validate = _checkProductNameInKeywords(product_name, validate_keywords);
            if (!validate) { continue; }


            // 쇼핑몰별 최저가
            if (products_list[i].lowMallList) {
                row_price = parseInt(products_list[i].lowMallList[0].price);
                row_market = products_list[i].lowMallList[0].name;
            // 스마트스토어
            } else {
                row_price = products_list[i].lowPrice;
                row_market = products_list[i].mallName;
            }


            // 출시예정 == ((price == 0) or 마켓이름 공백일때) 추정
            if (row_price === 0 && row_market === '') {
                continue;
            }

            // 백플레이트, 수냉쿨러, 팬 등 업자들 카테고리 미스 필터용(임시방편)
            if (type == 2 && row_price < 500000) {
                continue;
            }

            if (save_lower_price > row_price) {
                save_lower_price = row_price;
                save_lower_market = row_market;
                same_price_cnt = 0;
            } else if (save_lower_price === row_price) {
                same_price_cnt++;
            }
        }

        if (Number.MAX_SAFE_INTEGER === save_lower_price) {
            save_lower_price = 0;   
        }

        if (same_price_cnt > 0) {
            save_lower_market += ` 외 ${same_price_cnt}건`; 
        }

        console.log(`${res.data.shoppingResult.query} / ${save_lower_price} / ${save_lower_market}`);
        
        return {lower_price: save_lower_price, lower_market: save_lower_market};
    
    } catch(e) {
        console.log(`_getRowerPrice Error.`);
        console.log(`search name :: ${res.data.shoppingResult.query}`);
        console.log(e);
        throw new Error();
    }
}


/**
 * 
 * @param {String} product_name 
 * @param {String} validate_keywords 
 * @returns 
 */
function _checkProductNameInKeywords(product_name, validate_keywords) {
    const check_keywords = product_name.toLowerCase();
    const validate_keywords_length = validate_keywords.length;

    /**
     * '/' (OR조건) 체크
     * '!' 체크
     */
    for(let i = 0; i<validate_keywords_length; i++) {
        const lower_validate_keyword = validate_keywords[i].toLowerCase();

        if (lower_validate_keyword[0] === '!') {
            if (check_keywords.includes(lower_validate_keyword.substring(1))) { return false; }
            continue;
        } 
        
        if (lower_validate_keyword.includes('/')) {
            const orCheckKeyword = lower_validate_keyword.split('/');
            const orCheckRes = _orCheck(check_keywords, orCheckKeyword);
            if (!orCheckRes) { return false; }

        } else if (!check_keywords.includes(lower_validate_keyword)) {
            return false;
        }
    }

    return true;
}

/**
 * @param {String} check_keywords 
 * @param {Array<String>} check_list
 * @returns {boolean}
 */
function _orCheck(check_keywords, check_list) {
    for(const check_row of check_list) {
        if (check_keywords.includes(check_row)) {
            return true;
        }
    }

    return false;
}