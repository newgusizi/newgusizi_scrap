var axios = require('axios');

module.exports.CustomHttpRequest = class CustomHttpRequest {
    constructor(url, body) {
        this.url = url;
        this.body = body;
        this.header = {};
    };

    
    setHeader(header) {
        this.header = header;
    }

    
}