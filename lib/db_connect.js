const mysql2 = require('mysql2');
const dotenv = require("dotenv");
const connectionLimit = 10;

dotenv.config();

console.log('connect DB info');
console.log(`HOST:: ${process.env.DB_HOST}`);
console.log(`DB_USER:: ${process.env.DB_USER}`);
console.log(`DB_PASSWORD:: ${process.env.DB_PASSWORD}`);
console.log(`DB_SCHEME:: ${process.env.DB_SCHEME}`);

// 옵션이 제대로 들어갔지만 비밀번호 사용x면 gcp 방화벽이나 mysql 접속계정의 ip업데이트
const pool = mysql2.createPool({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_SCHEME,
    connectionLimit: connectionLimit,
    waitForConnections: true,
});

const promisePool = pool.promise();



module.exports.getSearchList = async function() {
    const conn = await promisePool.getConnection();
    try {
        const query = ` SELECT   search_name, 
                                type, 
                                keywords 
                        FROM search;`;

        const result = (await conn.query(query))[0];
        console.log(query);
        return result;
    } catch(e) {
        console.log('getSearchList Exception.');
        console.log(e);
        return e;
    } finally {
        console.log(new Date().toLocaleString());
        conn.release();
    }
}


module.exports.insertSearch = async function(search_name, price, market, type) {
    const conn = await promisePool.getConnection();
    try {
        const query = `INSERT INTO newgusizi.prices(search_name, price, update_date, market, type)
                                            values('${search_name}', ${price}, now(), '${market}', ${type});`;
        await conn.query(query);
        console.log(query);
    } catch(e) {
        console.log('insertSearchList Exception.');
        console.log(e);
    } finally {
        console.log(new Date().toLocaleString());
        conn.release();
    }
}


module.exports.updateSearchList = async function(search_name, price, market) {
    const conn = await promisePool.getConnection();
    try {
        const query =   `UPDATE newgusizi.prices
                         SET price = ${price}, market = '${market}', update_date = now()
                         WHERE search_name = '${search_name}'`;
        const result = (await conn.query(query))[0];
        console.log(query);
        return result;
    } catch(e) {
        console.log('updateSearchList Exception.');
        console.log(e);
        return e;
    } finally {
        console.log(new Date().toLocaleString());
        conn.release();
    }
}