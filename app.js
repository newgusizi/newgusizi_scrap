const express = require('express');
const schedule = require('node-schedule');
const scrap = require('./scrap_modules/scrap_module');
const app = express();
const port = 4651;

// TODO:: 추후 관리서버 추가
// const scrap = require('./scrap_router/(no use)scrap_router');
// app.use('/scrap', scrap);

app.listen(port, async () => {
    console.log(`scrap server port: ${port}`);
    console.log(`scrap module start: ${new Date()}`);
    await scrap.updatePrices();
    // try {
    //     schedule.scheduleJob('0 30 0 * * *', async () => {
    //         console.log(`scrap module start: ${new Date()}`);
    //         await scrap.updatePrices();
    //         console.log('scrap success!');
    //     });
    // } catch(e) {
    //     console.log('scrap failed!');
    //     console.log(`Exception:: ${e}`);
    // }
});