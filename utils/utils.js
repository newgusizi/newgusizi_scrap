
// 참고
// https://kkangdda.tistory.com/77
// https://velog.io/@dami/JS-Microtask-Queue
exports.sleep = async (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
